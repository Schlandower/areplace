if(!String.prototype.areplace) {
	String.prototype.areplace = function (search, replacement, all = '') {
		var to = typeof this;
		if(to !== 'string') {
			throw TypeError('A string required as input for replacement, "' + to + '" was given!\n');
		}
		if(!Array.isArray(search)) {
			throw TypeError('Array required, "' + typeof this + '" was given!\n');
		}
		to = typeof replacement;
		if(!Array.isArray(replacement) || to !== 'string') {
			throw TypeError('"replacement" needs to be an Array or a String, "' + to + '" was given!');
		}
		var i, j, rv = this, s = [], sz;
		search.forEach((elem, key) => {
			elem = elem.replace(/\\/g, "\\\\");
			elem = elem.replace(/\|/g, "\\|");
			elem = elem.replace(/\+/g, "\\+");
			elem = elem.replace(/\[/g, "\\[");
			elem = elem.replace(/\]/g, "\\]");
			elem = elem.replace(/\(/g, "\\(");
			elem = elem.replace(/\)/g, "\\)");
			elem = elem.replace(/\{/g, "\\{");
			elem = elem.replace(/\}/g, "\\}");
			elem = elem.replace(/\?/g, "\\?");
			elem = elem.replace(/\//g, "\\/");
			elem = elem.replace(/\*/g, "\\*");
			elem = elem.replace(/\^/g, "\\^");
			elem = elem.replace(/\./g, "\\.");
			elem = elem.replace(/\$/g, "\\$");
			s[key] = elem;
		});
		search = s;
		if(Array.isArray(replacement) && search.length === replacement.length) {
			for(i = 0, j = search.length; i < j; i++) {
				sz = new RegExp(search[i], all);
				rv =  rv.replace(sz, replacement[i]);
			}
			return rv;
		} else if(typeof replacement === 'string') {
			for(i = 0, j = search.length; i < j; i++) {
				sz = new RegExp(search[i], all);
				rv =  rv.replace(sz, replacement);
			}
			return rv;
		} else {
			return this;
		}
	};
}
