Similar to string.replace, except "search" is an array, and "replace" can be an array or a string.

Example:

var str = 'This fuzzy chipmunk can fly';
str = str.areplace(['fuzzy', 'fly'], ['happy', 'run'], 'gi');

The new string would read 'This happy chipmunk can run'.

The 'gi' is for global replacement, and case insensitive.